﻿using Configuration;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

public class test : MonoBehaviour
{
   public  bool isActive = false;
    // Start is called before the first frame update
    [Button("load scene")]
    private void LoadScene()
    {
        ScreenManager.LoadSceneWithLoading("HomeScene");
        Logger.Log(Config.AssetBundlePath);
    }

    private void Awake()
    {
        LoadScene();
    }
}
