using System;
using ByteSheep.Events;
using DG.Tweening;
using HedgehogTeam.EasyTouch;
using ModuleSound;
using NaughtyAttributes;
using UniRx.Async;
using UnityEngine;

namespace Buttons
{
    public class Button : MonoBehaviour, IButton
    {
        #region Properties

        public AdvancedEvent clickEvent;
        public Action taskClick;
        public Action taskCLickComplete;
        public bool isPlaySoundOnClick;
        [ShowIf("isPlaySoundOnClick")] public AudioSeStruct audioClip;
        public bool isUsingPunch;
        [ShowIf("isUsingPunch")] public bool customPressAnimationSetting = true;

        [ShowIf("customPressAnimationSetting")] [BoxGroup("Press")]
        public float pressScale = 0.95f;

        [ShowIf("customPressAnimationSetting")] [BoxGroup("Press")]
        public float durationPress = 0.05f;

        [ShowIf("customPressAnimationSetting")] [BoxGroup("Press")]
        public Ease easePress = Ease.OutCubic;

        [ShowIf("isUsingPunch")] public bool customReleaseAnimationSetting;

        [ShowIf("customReleaseAnimationSetting")] [BoxGroup("Release")]
        public Vector2 releaseScale = new Vector2(0.05f, 0.05f);

        [ShowIf("customReleaseAnimationSetting")] [BoxGroup("Release")]
        public float elasticity = 0.5f;

        [ShowIf("customReleaseAnimationSetting")] [BoxGroup("Release")]
        public int vibrato = 1;

        [ShowIf("customReleaseAnimationSetting")] [BoxGroup("Release")]
        public float durationRelease = 0.2f;

        [ShowIf("customReleaseAnimationSetting")] [BoxGroup("Release")]
        public AnimationCurve releaseAniamtionCurve;

        private Sequence _sequencePress;
        private Sequence _sequenceRelease;
        private bool _isDonePress;
        private Gesture _tempGesture;
        private bool IsSkipPlaySound { get; set; }
        private Vector3 _positionOnTouch;
        private bool IsPress { get; set; }

        #endregion

        #region Method

        public void OnEnable()
        {
            EasyTouch.On_TouchStart += On_TouchStart;
            EasyTouch.On_TouchDown += On_TouchDown;
            EasyTouch.On_TouchUp += On_TouchUp;
        }

        public void OnDisable()
        {
            UnsubscribeEvent();
        }

        private void UnsubscribeEvent()
        {
            EasyTouch.On_TouchStart -= On_TouchStart;
            EasyTouch.On_TouchDown -= On_TouchDown;
            EasyTouch.On_TouchUp -= On_TouchUp;
        }

        private void CreateSequencePressButton(float value, float duration, Action sequenceStart = null,
            Action sequenceComplete = null)
        {
            _sequencePress = DOTween.Sequence();
            _sequencePress.Append(transform.DOScale(value, duration).SetEase(easePress));
            _sequencePress.SetLoops(1);
            _sequencePress.SetRelative(false);
            _sequencePress.OnPlay(() => { sequenceStart?.Invoke(); });

            _sequencePress.OnComplete(delegate
            {
                sequenceComplete?.Invoke();
                _sequencePress.Kill();
            });
            _sequencePress.Play();
        }

        private void CreateSequence(Vector3 value, float duration, int vibrate, float elastic,
            AnimationCurve easeCurve, Action sequencePlay = null, Action sequenceComplete = null)
        {
            _sequenceRelease = DOTween.Sequence();
            _sequenceRelease.Append(transform.DOPunchScale(value, duration, vibrate, elastic).SetEase(easeCurve));
            _sequenceRelease.SetLoops(1);
            _sequenceRelease.SetRelative(false);
            _sequenceRelease.OnPlay(() => { sequencePlay?.Invoke(); });

            _sequenceRelease.OnComplete(delegate
            {
                sequenceComplete?.Invoke();
                _sequenceRelease.Kill();
            });
            _sequenceRelease.Play();
        }

        private async void WaitPressDone(Action action)
        {
            await UniTask.WaitUntil(() => IsPress == false && _isDonePress);
            action?.Invoke();
        }

        public virtual void OnClick()
        {
            if (_tempGesture == null) return;
            if (!(Math.Abs(_tempGesture.GetTouchToWorldPoint(100).x - _positionOnTouch.x) > 1f) &&
                !(Math.Abs(_tempGesture.GetTouchToWorldPoint(100).y - _positionOnTouch.y) > 1f)) return;
            IsSkipPlaySound = true;
        }

        private void OnDestroy()
        {
            UnsubscribeEvent();

            if (!isUsingPunch) return;
            if (_sequencePress != null && _sequencePress.IsPlaying() && IsPress)
            {
                _sequencePress?.Complete();
            }

            //Skip sequence release.
        }

        public void CommingSoon()
        {
            //PopupImagniationController.Instance.CommingSoon ();
        }

        #endregion

        #region Event Touch

        private void On_TouchStart(Gesture gesture)
        {
            if (!gesture.isOverGui || gesture.pickedUIElement != gameObject) return;
            _tempGesture = gesture;
            _positionOnTouch = gesture.GetTouchToWorldPoint(100);
        }

        private void On_TouchDown(Gesture gesture)
        {
            if (gesture.pickedUIElement == gameObject)
            {
                TaskDown();
            }
        }

        private void On_TouchUp(Gesture gesture)
        {
            if (gesture.pickedUIElement != gameObject) return;
            _tempGesture = gesture;
            IsSkipPlaySound = false;
            TaskUp();
        }

        private void TaskDown()
        {
            if (!isUsingPunch || !customPressAnimationSetting || IsPress) return;
            IsPress = true;
            CreateSequencePressButton(pressScale, durationPress, () => _isDonePress = false,
                () => _isDonePress = true);
        }

        private void TaskUp()
        {
            if (isUsingPunch && customReleaseAnimationSetting && IsPress)
            {
                IsPress = false;
                WaitPressDone(() =>
                {
                    try
                    {
                        transform.localScale = new Vector3(1, 1, 1);
                        CreateSequence(releaseScale, durationRelease, vibrato, elasticity, releaseAniamtionCurve, () =>
                        {
                            OnClick();
                            clickEvent?.Invoke();
                            taskClick?.Invoke();
                            if (isPlaySoundOnClick && !IsSkipPlaySound)
                            {
                                SoundManager.Instance.PlaySoundEffect(audioClip.name, audioClip.volume, audioClip.timeDelay);
                            }
                        }, () => { taskCLickComplete?.Invoke(); });
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                });
            }
            else
            {
                OnClick();
                clickEvent?.Invoke();
                taskClick?.Invoke();
                if (isPlaySoundOnClick && !IsSkipPlaySound)
                {
                    SoundManager.Instance.PlaySoundEffect(audioClip.name, audioClip.volume, audioClip.timeDelay);
                }
            }
        }

        #endregion
    }
}