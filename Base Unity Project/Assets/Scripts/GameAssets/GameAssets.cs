﻿using System.Collections.Generic;
using UnityEngine;

public class GameAssets : Singleton<GameAssets>
{
	#region Properties
	[SerializeField]
	private LoggerConfig loggerConfig;
	[SerializeField]
	private Dictionary<string, AssetBundle> assetBundleDictionary = new Dictionary<string, AssetBundle>();
	#endregion
	#region Getter and Setter
	public LoggerConfig LoggerConfig { get => loggerConfig; set => loggerConfig = value; }
	private Dictionary<string, AssetBundle> AssetBundleDictionary => assetBundleDictionary;
		
	#endregion

	#region Method

	public void AddAssetBundle(AssetBundlePack assetBundlePack,AssetBundle assetBundle)
	{
		AssetBundleDictionary.Add(assetBundlePack.BunderName,assetBundle);
	}
	public T LoadAsset<T>(AssetBundle bundle, string assetName) where T : Object {
		var asset = bundle.LoadAsset<T>(assetName);
		if (asset == null) Logger.LogError("Failed to load Asset {0} from {1} ({2})");
		return asset;
	}
	

	#endregion
}