using System.Collections;
using Configuration;
using Language_Manager;
using UnityEngine;
using UnityEngine.UI;

public class LoadingManager : MonoBehaviour
{

    #region Properties

    [SerializeField] private Slider loadingBar;
    [SerializeField] private LanguageTextManager txtTip;
   // [SerializeField] private LanguageTextManager txtStatus;
    [SerializeField] private float timeFakeLoading = 1f;
    [SerializeField] private int tipNumber = 26;
    [SerializeField] private float tipDisplayTime = 1f;
    public static LoadingManager Instance { get; private set; }

    public bool isLoadingFinish;
    #endregion

    #region Method

    private void Awake()
    {
        Instance = this;
    }

    private float _loadingBar;

    public void StartLoading()
    {
        loadingBar.value = 0;
        StartCoroutine(RunLoadingBar());
        InvokeRepeating(nameof(TipDisplay),0f,tipDisplayTime);
    }

    private IEnumerator RunLoadingBar()
    {
        isLoadingFinish = false;
        yield return new WaitForEndOfFrame();
        while (loadingBar.value<1)
        {
            loadingBar.value += Time.deltaTime / timeFakeLoading;
            yield return null;
        }

        yield return null;
        isLoadingFinish = true;
    }

    private void TipDisplay()
    {
        txtTip.SetLanguageId(Config.GetTipKeyLanguage( Random.Range(1, tipNumber+1)));
    }
    #endregion
}