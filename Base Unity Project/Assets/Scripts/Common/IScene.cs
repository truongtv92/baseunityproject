﻿public interface IScene
{
       void FadeIn();
       void FadeOut();
}
