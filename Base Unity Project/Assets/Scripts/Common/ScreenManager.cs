﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;
using static UniRx.Observable;

public static class ScreenManager
{
    private static readonly List<string> SceneList = new List<string>();

    #region Public Method

    public static void LoadSceneWithLoading(string newScene)
    {
        FromCoroutine(StartLoadLoadingScene).SelectMany(() => StartLoadSceneWithLoading(newScene, () => { }))
            .Subscribe();
    }



    #endregion

    #region Private Method

    private static IEnumerator StartLoadSceneWithLoading(string name, Action completeAction = null)
    {
        LoadingManager.Instance.StartLoading();
        for (var i = SceneList.Count - 1; i >= 0; i--)
        {
            if (!SceneList[i].Equals("LoadingScene")) UnloadScene(SceneList[i]);
        }
        LoadSceneAdditive(name, asyncOperation =>
        {
            asyncOperation.allowSceneActivation = true;
        });
        while (!LoadingManager.Instance.isLoadingFinish)
        {
            yield return null;
        }
        yield return new WaitForSeconds(0.2f);
        LoadingManager.Instance.GetComponent<SceneCustom>().FadeOut();
        while (!LoadingManager.Instance.GetComponent<SceneCustom>().IsFadeOutDone)
        {
            yield return null;
        }
        yield return new WaitForSeconds(0.2f);
        UnloadScene("LoadingScene");
        yield return null;

        SceneCustom.LastScene.FadeIn();
        while (!SceneCustom.LastScene.IsFadeInDone)
        {
            yield return null;
        }

        yield return null;
        completeAction?.Invoke();
    }

    public static void LoadSceneSingle(string name, Action<AsyncOperation> completeAction = null)
    {
        FromCoroutine(() => LoadLevel(name, LoadSceneMode.Single, completeAction)).Subscribe();
    }

    private static void LoadSceneAdditive(string name, Action<AsyncOperation> completeAction = null)
    {
        FromCoroutine(() => LoadLevel(name, LoadSceneMode.Additive, completeAction)).Subscribe();
    }

    private static void UnloadScene(string name)
    {
        SceneManager.UnloadSceneAsync(name);
        SceneList.Remove(name);
    }

    private static IEnumerator LoadLevel(string name, LoadSceneMode loadSceneMode,
        Action<AsyncOperation> completeAction = null)
    {
        yield return new WaitForEndOfFrame();
        var asyncOperation = SceneManager.LoadSceneAsync(name, loadSceneMode);
        if (completeAction != null)
            asyncOperation.completed += completeAction;
        yield return asyncOperation;
        while (!asyncOperation.isDone)
        {
            yield return null;
        }

        SceneList.Add(name);
        if (completeAction == null)
            asyncOperation.allowSceneActivation = true;
        else completeAction.Invoke(asyncOperation);
    }


    private static IEnumerator StartLoadLoadingScene()
    {
        yield return new WaitForEndOfFrame();
        var sceneCustoms = Object.FindObjectsOfType<SceneCustom>();
        foreach (var sceneCustom in sceneCustoms)
        {
            sceneCustom.FadeOut();
            Logger.Log(sceneCustom.gameObject.name);
        }

        AsyncOperation loadingOperation = null;
       
        var  result = false;
        while (result == false)
        {
            result = sceneCustoms.Aggregate(true, (current, sceneCustom) =>
            {
                Logger.Log(sceneCustom.gameObject.name+" has transparent = "+sceneCustom.GetComponent<CanvasGroup>().alpha);
                return current && sceneCustom.IsFadeOutDone;
            });
            yield return null;
        }
        yield return null;
        LoadSceneAdditive("LoadingScene", asyncOperation =>
        {
            loadingOperation = asyncOperation;
            loadingOperation.allowSceneActivation = false;
        });
        while (loadingOperation==null)
        {
            yield return null;
        }

        loadingOperation.allowSceneActivation = true;
        SceneCustom.LastScene.FadeIn();
        while (!SceneCustom.LastScene.IsFadeInDone)
        {
            yield return null;
        }

        yield return null;
    }

    #endregion
    
}