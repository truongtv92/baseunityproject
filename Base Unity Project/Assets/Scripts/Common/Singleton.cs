﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Component
{
	private static T instance;
	public static T Instance
	{
		get {
			if (instance != null) return instance;
			instance = (T) FindObjectOfType (typeof (T));
			if (instance == null)
			{
				Logger.LogError (typeof (T).Name + "not found");
			}
			return instance;
		}
	}

	protected virtual void Awake ()
	{
		if (!CheckInstance ())
			Destroy (this);
	}

	private bool CheckInstance ()
	{
		return this == Instance;
	}

}