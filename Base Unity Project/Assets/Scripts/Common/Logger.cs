﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;
//using UnityEngine;


public static class Logger
{
	#region Properties
	//public 
	#endregion
	#region Methods

	private static bool IsShowLog ()
	{
#if UNITY_EDITOR
		return GameAssets.Instance.LoggerConfig.LoggerType == LoggerConfig.LogType.All ||
		       GameAssets.Instance.LoggerConfig.LoggerType == LoggerConfig.LogType.EditorOnly;
#elif UNITY_ANDROID
		return GameAssets.Instance.LoggerConfig.LoggerType == LoggerConfig.LogType.All||
			GameAssets.Instance.LoggerConfig.LoggerType == LoggerConfig.LogType.AndroidOnly||
			GameAssets.Instance.LoggerConfig.LoggerType == LoggerConfig.LogType.MobileOnly;
#elif UNITY_IOS
		return GameAssets.Instance.LoggerConfig.LoggerType == LoggerConfig.LogType.All||
			GameAssets.Instance.LoggerConfig.LoggerType == LoggerConfig.LogType.IosOnly||
			GameAssets.Instance.LoggerConfig.LoggerType == LoggerConfig.LogType.MobileOnly;
#endif
	}
	public static void Log (object message, Object context)
	{
		if (IsShowLog ())
			Debug.Log (GameAssets.Instance.LoggerConfig.EditorLogPrefix + message, context);
	}

	public static void Log (object message)
	{
		if (IsShowLog ())
			Debug.Log (GameAssets.Instance.LoggerConfig.EditorLogPrefix + message);
	}

	public static void LogError (object message, Object context)
	{
		if (IsShowLog ())
			Debug.LogError (GameAssets.Instance.LoggerConfig.EditorLogPrefix + message, context);
	}

	public static void LogError (object message)
	{
		if (IsShowLog ())
			Debug.LogError (GameAssets.Instance.LoggerConfig.EditorLogPrefix + message);
	}

	public static void LogErrorFormat (string format, params object[] args)
	{
		if (IsShowLog ())
			Debug.LogErrorFormat (GameAssets.Instance.LoggerConfig.EditorLogPrefix + format, args);
	}

	public static void LogErrorFormat (Object context, string format, params object[] args)
	{
		if (IsShowLog ())
			Debug.LogErrorFormat (GameAssets.Instance.LoggerConfig.EditorLogPrefix + context, format, args);
	}

	public static void LogException (Exception exception, Object context)
	{
		if (IsShowLog ())
			Debug.LogException (exception, context);
	}

	public static void LogException (Exception exception)
	{
		if (IsShowLog ())
			Debug.Log (exception);
	}

	public static void LogFormat (Object context, string format, params object[] args)
	{
		if (IsShowLog ())
			Debug.LogFormat (context, GameAssets.Instance.LoggerConfig.EditorLogPrefix + format, args);
	}

	public static void LogFormat (string format, params object[] args)
	{
		if (IsShowLog ())
			Debug.LogFormat (GameAssets.Instance.LoggerConfig.EditorLogPrefix + format, args);
	}

	public static void LogWarning (object message)
	{
		if (IsShowLog ())
			Debug.LogWarning (GameAssets.Instance.LoggerConfig.EditorLogPrefix + message);
	}

	public static void LogWarning (object message, Object context)
	{
		if (IsShowLog ())
			Debug.LogWarning (GameAssets.Instance.LoggerConfig.EditorLogPrefix + message, context);
	}

	public static void LogWarningFormat (string format, params object[] args)
	{
		if (IsShowLog ())
			Debug.LogWarningFormat (GameAssets.Instance.LoggerConfig.EditorLogPrefix + format,args);
	}

	public static void LogWarningFormat (Object context, string format, params object[] args)
	{
		if (IsShowLog ())
			Debug.LogWarningFormat (context, GameAssets.Instance.LoggerConfig.EditorLogPrefix + format,args);
	}
	#endregion
}
