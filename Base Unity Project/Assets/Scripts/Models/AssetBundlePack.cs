﻿using UnityEngine;

public class AssetBundlePack 
{
	private AssetBundle _bundle;
	public AssetBundlePack (string url, string bunderName, int version,long size)
	{
		Url = url;
		BunderName = bunderName;
		Version = version;
		Size = size;
	}
	public string Url { get; } = "";
	public string BunderName { get; } = "";
	public int Version { get; }
	private long Size { get; }
	public byte[] Bytes { get; set; }
	
}
