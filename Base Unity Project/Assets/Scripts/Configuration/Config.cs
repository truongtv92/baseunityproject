﻿using UnityEngine;

namespace Configuration
{
	public static class Config 
	{
		#region Static Properties

		public static readonly string AssetBundlePath = Application.streamingAssetsPath+"/resource/data/";
		
		#endregion

		#region Const Value

		private const string TIP_TXT = "TxtLoadingTip";


		#endregion

		#region Method

		public static string GetTipKeyLanguage(int value)
		{
			Logger.Log(TIP_TXT + value);
			return TIP_TXT + value;
		}

		#endregion
	}
}
