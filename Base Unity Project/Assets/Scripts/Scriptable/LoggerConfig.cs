﻿using UnityEngine;
[CreateAssetMenu (fileName = "LoggerConfig", menuName = "GameConfiguration/LoggerConfig", order = 1)]
public class LoggerConfig : ScriptableObject
{
	#region Properties
	[SerializeField] private LogType loggerType = LogType.All;
	[SerializeField] private string editorLogPrefix = "";
	[SerializeField] private string androidLogPrefix = "";
	[SerializeField] private string iosLogPrefix = "";
	#endregion
	#region Getter and Setter
	public string EditorLogPrefix { get => editorLogPrefix; set => editorLogPrefix = value; }
	public string AndroidLogPrefix { get => androidLogPrefix; set => androidLogPrefix = value; }
	public string IosLogPrefix { get => iosLogPrefix; set => iosLogPrefix = value; }
	public LogType LoggerType { get => loggerType; set => loggerType = value; }
	#endregion
	public enum LogType
	{
		None = 0,
		EditorOnly = 1,
		AndroidOnly = 2,
		IosOnly = 3,
		MobileOnly = 4,
		All = 5
	}
}

