﻿using System;
using Buttons;
using Language_Manager;
using UnityEngine;

namespace Popups
{
    public class YesNoPopup : PopUp
    {
        #region Properties

        [SerializeField]private LanguageTextManager txtTitle;
        [SerializeField]private LanguageTextManager txtDescription;
        [SerializeField]private LanguageTextManager btnYesText;
        [SerializeField]private LanguageTextManager btnNoText;
        [SerializeField]private Button btnYes;
        [SerializeField]private Button btnNo;
        #endregion

        #region Method
        public void ShowMessage(string title, string description, string btnYesName, Action btnYesAction,object[] param=null)
        {
            txtTitle.SetLanguageId(title);
            txtDescription.SetLanguageId(description,param);
            btnYesText.SetLanguageId(btnYesName);
            btnYes.taskCLickComplete = btnYesAction;
            btnNo.taskCLickComplete = ClosePopUp;
            ShowPopUp();
        }
        public void ShowMessage(string title, string description, string btnYesName,string btnNoName, Action btnYesAction,Action btnNoAction,object[] param=null)
        {
            txtTitle.SetLanguageId(title);
            txtDescription.SetLanguageId(description,param);
            btnYesText.SetLanguageId(btnYesName);
            btnYes.taskCLickComplete = btnYesAction;
            btnNoText.SetLanguageId(btnNoName);
            btnNo.taskCLickComplete = btnNoAction;
            ShowPopUp();
        }

        #endregion
        
    }
}
