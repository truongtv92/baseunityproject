using System;
using Buttons;
using Language_Manager;
using UnityEngine;

namespace Popups
{
    public class NotificationPopUp : PopUp
    {
        #region Properties

        [SerializeField]private LanguageTextManager txtTitle;
        [SerializeField]private LanguageTextManager txtDescription;
        [SerializeField]private LanguageTextManager btnText;
        [SerializeField] private Button btn;
       // private Action _popupAction;
        #endregion
        #region Method

        public void ShowMessage(string title, string description, string btnName, Action btnAction,object[] param =null)
        {
            txtTitle.SetLanguageId(title);
            txtDescription.SetLanguageId(description,param);
            btnText.SetLanguageId(btnName);
            btn.taskCLickComplete = btnAction;
            ShowPopUp();
        }

        public void ShowMessage(string title, string description,object[] param=null)
        {
            txtTitle.SetLanguageId(title);
            txtDescription.SetLanguageId(description,param);
             btn.taskCLickComplete = ClosePopUp;
            ShowPopUp();
        }
        #endregion
    }
}