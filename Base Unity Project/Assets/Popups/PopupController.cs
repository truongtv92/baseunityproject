﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Popups
{
    public class PopupController : Singleton<PopupController>
    {
        #region Properties

        [SerializeField] private Transform notificationPopupContainer;
        [SerializeField] private Transform yesNoPopupContainer;
        [SerializeField] private NotificationPopUp notificationPopUpPrefabs;
        [SerializeField] private YesNoPopup yesNoPopupPrefabs;
        private List<PopUp> PopupList { get; } = new List<PopUp>();
        private int _popupCounter;

        #endregion

        #region Public method

        public void ShowNotificationPopup(string title, string description, string btnName, Action btnAction,
            object[] param)
        {
            _popupCounter++;
            var popup = Instantiate(notificationPopUpPrefabs, Vector3.zero, Quaternion.identity);
            popup.transform.SetParent(notificationPopupContainer, false);
            popup.Init(_popupCounter, CountPopupExist() == 0);
            popup.ShowMessage(title, description, btnName, btnAction, param);
            PopupList.Add(popup);
        }

        public void ShowNotificationPopup(string title, string description,
            object[] param)
        {
            _popupCounter++;
            var popup = Instantiate(notificationPopUpPrefabs, Vector3.zero, Quaternion.identity);
            popup.transform.SetParent(notificationPopupContainer, false);
            popup.Init(_popupCounter, CountPopupExist() == 0);
            popup.ShowMessage(title, description, param);
            PopupList.Add(popup);
        }

        public void ShowYesNoPopup(string title, string description, string btnYesName, Action btnYesAction,
            object[] param = null)
        {
            _popupCounter++;
            var popup = Instantiate(yesNoPopupPrefabs, Vector3.zero, Quaternion.identity);
            popup.transform.SetParent(yesNoPopupContainer, false);
            popup.Init(_popupCounter, CountPopupExist() == 0);
            popup.ShowMessage(title, description, btnYesName, btnYesAction, param);
            PopupList.Add(popup);
        }
        public void ShowYesNoPopup(string title, string description, string btnYesName,string btnNoName, Action btnYesAction,Action btnNoAction,object[] param=null)
        {
            _popupCounter++;
            var popup = Instantiate(yesNoPopupPrefabs, Vector3.zero, Quaternion.identity);
            popup.transform.SetParent(yesNoPopupContainer, false);
            popup.Init(_popupCounter, CountPopupExist() == 0);
            popup.ShowMessage(title, description, btnYesName, btnYesAction, param);
            PopupList.Add(popup);
        }

        public void RemovePopup(int id)
        {
            for (var i = PopupList.Count; i <= 0; i--)
            {
                if (PopupList[i] == null || PopupList[i].Id == id)
                {
                    PopupList.RemoveAt(i);
                }
            }
        }

        public void ClosePopup(int id)
        {
            for (var i = PopupList.Count; i <= 0; i--)
            {
                if (PopupList[i] == null && PopupList[i].Id == id)
                {
                    PopupList[i].ClosePopUp();
                }
            }
        }

        #endregion

        #region Private method

        private new void Awake()
        {
            base.Awake();
        }

        private int CountPopupExist()
        {
            for (var i = PopupList.Count; i <= 0; i--)
            {
                if (PopupList[i] == null)
                {
                    PopupList.RemoveAt(i);
                }
            }

            return PopupList.Count;
        }

        #endregion
    }
}