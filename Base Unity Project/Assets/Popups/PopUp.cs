using UnityEngine;
using UnityEngine.UI;

namespace Popups
{
    public class PopUp : MonoBehaviour, IPopUp
    {
        [SerializeField] private Image blackBackground;
        public int Id { get; private set; }

        public virtual void Init(int id, bool isHaveBlackBg)
        {
            Id = id;
            if (blackBackground != null)
                blackBackground.enabled = isHaveBlackBg;
        }

        protected virtual void ShowPopUp()
        {
            //
        }

        public virtual void ClosePopUp()
        {
            PopupController.Instance.RemovePopup(Id);
        }
    }
}