﻿using System.IO;
using System.Threading.Tasks;
using Configuration;

namespace AssetBundles
{
	public static class AssetBundleSaver
	{
		public static void SaveAssetBundleToFile (AssetBundlePack assetBundlePack, byte[] bytes)
		{
			Task.Run (() =>
			{
				if (File.Exists(Config.AssetBundlePath + assetBundlePack.BunderName))
				{
					File.Delete(Config.AssetBundlePath + assetBundlePack.BunderName);
				}
				File.WriteAllBytes (Config.AssetBundlePath+assetBundlePack.BunderName, bytes);
			});
			Logger.Log("now");
		}
	}
}
