﻿using System.IO;
using Configuration;
using UnityEngine;

namespace AssetBundles
{
	public static class AssetBundleLoader
	{
		
		public static AssetBundle GetAssetBundleLocal (AssetBundlePack assetBundlePack)
		{
			return IsAssetBundleExist(assetBundlePack) ? AssetBundle.LoadFromFile (Config.AssetBundlePath + assetBundlePack.BunderName) : null;
		}

		public static bool IsAssetBundleExist(AssetBundlePack assetBundlePack)
		{
			return  File.Exists(Config.AssetBundlePath+assetBundlePack.BunderName);
		}

		public static AssetBundle GetAssetBundleFromBytes(byte[] stream)
		{
			return AssetBundle.LoadFromMemory(stream);
		}
	}
}
