﻿using AssetBundles.Event;
using UnityEngine;

namespace AssetBundles
{
	public class AssetBundleControllerUi : MonoBehaviour
	{
		#region Properties

	
		#endregion
		#region Method
		#region Unity Implement
		private void OnEnable()
		{
			AssetBundleManager.downloadStartHandler += OnDownloadStartHandler;
			AssetBundleManager.downloadInProgressHandler += OnDownloadInProgressHandler;
			AssetBundleManager.downloadFinishHandler += OnDownloadFinishHandler;
			AssetBundleManager.downloadErrorHandler += OnDownloadErrorHandler;
		}
		private void OnDisable()
		{
			AssetBundleManager.downloadStartHandler -= OnDownloadStartHandler;
			AssetBundleManager.downloadInProgressHandler -= OnDownloadInProgressHandler;
			AssetBundleManager.downloadFinishHandler -= OnDownloadFinishHandler;
			AssetBundleManager.downloadErrorHandler -= OnDownloadErrorHandler;
		}
		#endregion
		#region Listener
		private static void OnDownloadStartHandler(AssetBundleDownloadEventArgs e)
		{
			SetStartDownloadBundleUi(e.AssetBundlePack);
		}
		private static void OnDownloadInProgressHandler(AssetBundleDownloadEventArgs e)
		{
			SetDownloadingBundleUi(e.AssetBundlePack,e.Progress);
		}
		private static void OnDownloadFinishHandler(AssetBundleDownloadEventArgs e)
		{
			SetDownloadBundleFinishUi(e.AssetBundlePack);
		}

		private static void OnDownloadErrorHandler(AssetBundleDownloadEventArgs e)
		{
			SetDownloadBundleFailUi(e.AssetBundlePack,e.Error);
		}
		#endregion
		#region UI work

		private static void SetStartDownloadBundleUi (AssetBundlePack assetBundlePack)
		{
			Logger.Log (assetBundlePack.BunderName);
		}

		private static void SetDownloadingBundleUi (AssetBundlePack assetBundlePack, float progress)
		{
			Logger.Log (assetBundlePack.BunderName+" have downloading progress : "+ progress);
		}

		private static void SetDownloadBundleFinishUi (AssetBundlePack assetBundlePack)
		{
			Logger.Log (assetBundlePack.BunderName+" finish");
		}

		private static void SetDownloadBundleFailUi (AssetBundlePack assetBundlePack, string error)
		{
			Logger.Log (assetBundlePack.BunderName + " has error: "+error);
		}

		#endregion
		#endregion
	}
}
