﻿using System;

namespace AssetBundles.Event
{
    
    public class AssetBundleDownloadEventArgs : EventArgs
    {
        public AssetBundlePack AssetBundlePack { get; }
        public string Error { get; }
        public float Progress { get; }
        public AssetBundleDownloadEventArgs(AssetBundlePack assetBundlePack)
        {
            AssetBundlePack = assetBundlePack;
        }
        public AssetBundleDownloadEventArgs(AssetBundlePack assetBundlePack,float progress)
        {
            AssetBundlePack = assetBundlePack;
            Progress = progress;
        }
        public AssetBundleDownloadEventArgs(AssetBundlePack assetBundlePack,string error)
        {
            AssetBundlePack = assetBundlePack;
            Error = error;
        }
        
    }
    
}
