﻿using System;
using System.Collections;
using System.Collections.Generic;
using AssetBundles.Event;
using UniRx;
using UnityEngine;
using static UniRx.Observable;

namespace AssetBundles
{
	public delegate void AssetBundleDownloadEventHandler( AssetBundleDownloadEventArgs e);
	public static class AssetBundleManager
	{
		#region Properties
		public static event AssetBundleDownloadEventHandler downloadStartHandler;
		public static event AssetBundleDownloadEventHandler downloadInProgressHandler;
		public static event AssetBundleDownloadEventHandler downloadFinishHandler;
		public static event AssetBundleDownloadEventHandler downloadErrorHandler;
		#endregion
		#region Getter and Setter

		#endregion
		#region Method

		private static IDisposable disposeDownload;

		public static void DownloadAssetBundle(IEnumerable<AssetBundlePack> assetBundlePacks)
		{
			disposeDownload = FromCoroutine(() =>DownloadBundles(assetBundlePacks)).Subscribe();
			
		}

		private static IEnumerator DownloadBundles (IEnumerable<AssetBundlePack> assetBundlePacks)
		{
			foreach (var assetBundlePack in assetBundlePacks)
			{
				yield return FromCoroutine(()=>AssetBundleDownloader.DownloadAssetsBundle (assetBundlePack)).ToYieldInstruction();
			}
			yield return null;
		}
		public static bool CheckAssetsBundleValid (AssetBundlePack assetBundlePack)
		{
			return AssetBundleLoader.IsAssetBundleExist(assetBundlePack);
		}
		public static AssetBundle LoadAssetBundle (AssetBundlePack assetBundlePack)
		{
			return AssetBundleLoader.GetAssetBundleLocal(assetBundlePack);
		}

		#region Work with UI
		public static void StartDownloadBundleCallBack (AssetBundlePack assetBundlePack)
		{
			downloadStartHandler(new AssetBundleDownloadEventArgs(assetBundlePack));
		}
		public static void DownloadingBundleCallBack (AssetBundlePack assetBundlePack,float progress)
		{
			downloadInProgressHandler(new AssetBundleDownloadEventArgs(assetBundlePack, progress)) ;
		}
		public static void DownloadBundleFinishCallBack (AssetBundlePack assetBundlePack,byte[] result)
		{
			downloadFinishHandler(new AssetBundleDownloadEventArgs(assetBundlePack));
			AssetBundleSaver.SaveAssetBundleToFile(assetBundlePack,result);
		}
		public static void DownloadBundleFailCallBack (AssetBundlePack assetBundlePack,string error)
		{
			downloadErrorHandler(new AssetBundleDownloadEventArgs(assetBundlePack, error));
			disposeDownload.Dispose();
		}
		#endregion
		#endregion
	}
}
