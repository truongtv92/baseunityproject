﻿using System.Collections;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;

namespace AssetBundles
{
	public static class AssetBundleDownloader
	{
		public static IEnumerator DownloadAssetsBundle ( AssetBundlePack assetBundlePack)
		{
			var www = UnityWebRequest.Get (assetBundlePack.Url);
			www.downloadHandler = new DownloadHandlerBuffer ();
			StartDownloadBundleCallBack (assetBundlePack);
			www.SendWebRequest ();
			while (www.downloadProgress < 1)
			{
				DownloadingBundleCallBack (assetBundlePack, www.downloadProgress);
				yield return null;
			}
			yield return www;
			if (www.isDone)
			{
				if (string.IsNullOrEmpty (www.error))
				{
					var result = www.downloadHandler.data;
					DownloadBundleFinishCallBack (assetBundlePack, result);
				}
				else
				{
					DownloadBundleFailCallBack (assetBundlePack, www.error);
				}
			}
			yield return null;
		}

		private static void StartDownloadBundleCallBack (AssetBundlePack assetBundlePack)
		{
			AssetBundleManager.StartDownloadBundleCallBack(assetBundlePack);
		}
		private static void DownloadingBundleCallBack (AssetBundlePack assetBundlePack, float progress)
		{
			AssetBundleManager.DownloadingBundleCallBack(assetBundlePack, progress);
		}
		private static void DownloadBundleFinishCallBack (AssetBundlePack assetBundlePack, byte[] result)
		{
			AssetBundleManager.DownloadBundleFinishCallBack (assetBundlePack,result);
		}
		private static void DownloadBundleFailCallBack (AssetBundlePack assetBundlePack, string error)
		{
			AssetBundleManager.DownloadBundleFailCallBack (assetBundlePack,error);
		}
	}
}
