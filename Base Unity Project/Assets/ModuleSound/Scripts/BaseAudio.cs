﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

[Serializable]
public class BaseAudio : MonoBehaviour
{
	private ESoundPlayState _currentState;
	private AudioSource _source;
	private float _volume;
	private float _delay;
	private bool _isLoop;
	private float _fadeVolume;
	private float _fadeInTime;
	private float _fadeOutTime;
	private float _waitTimeFadeIn;
	private float _waitTimeFadeOut;
	private Action _onStartBefore;
	private Action _onStart;
	private Action _onCompleted;
	private Action _onCompletedAfter;
	private AudioClip _clip;
	private IDisposable _fadeDispose;
	private IDisposable _playDispose;
	private float _waitTime;

	#region GetSet

	public ESoundPlayState CurrentState { get => _currentState; set => _currentState = value; }
	public AudioSource Source { get => _source; set => _source = value; }
	public float Volume { get => _volume; set => _volume = Mathf.Clamp01 (value); }
	public float Delay { get => _delay; set => _delay = value; }
	public bool IsLoop { get => _isLoop; set => _isLoop = value; }
	protected float FadeVolume { get => _fadeVolume; set => _fadeVolume = value; }
	public float FadeInTime { get => _fadeInTime; set => _fadeInTime = value; }
	public float FadeOutTime { get => _fadeOutTime; set => _fadeOutTime = value; }
	protected float WaitTimeFadeIn { get => _waitTimeFadeIn; set => _waitTimeFadeIn = value; }
	public float WaitTimeFadeOut { get => _waitTimeFadeOut; set => _waitTimeFadeOut = value; }
	public Action OnStartBefore { get => _onStartBefore; set => _onStartBefore = value; }
	public Action OnStart { get => _onStart; set => _onStart = value; }
	public Action OnCompleted { get => _onCompleted; set => _onCompleted = value; }
	public Action OnCompletedAfter { get => _onCompletedAfter; set => _onCompletedAfter = value; }
	private IDisposable FadeDispose { get => _fadeDispose; set => _fadeDispose = value; }
	protected IDisposable PlayDispose { get => _playDispose; set => _playDispose = value; }
	public AudioClip Clip { get => _clip; set => _clip = value; }
	protected float WaitTime { get => _waitTime; set => _waitTime = value; }
	#endregion

	protected virtual void ChangeVolume ()
	{

	}

	public virtual void Stop ()
	{

	}

	protected void FadeIn (float fadeTime, float waitTime)
	{
		DisposeFade ();
		FadeDispose = Observable.FromMicroCoroutine (() => IeFadeIn (fadeTime, waitTime)).Subscribe ((m) => DisposeFade ());
	}

	public void FadeOut (float fadeTime, float waitTime)
	{
		DisposeFade ();
		FadeDispose = Observable.FromMicroCoroutine (() => IeFadeOut (fadeTime, waitTime)).Subscribe ((m) => DisposeFade ());
	}

	public void ChangeCurrentState(ESoundPlayState state)
	{
		CurrentState = state;
	}

	protected bool EqualCurrentState(ESoundPlayState state)
	{
		return CurrentState == state;
	}

	#region Fade
	private IEnumerator IeFadeIn (float fadeTime, float waitTime)
	{
		FadeVolume = 0;
		var time = 0f;
		while (time < waitTime)
		{
			time += Time.deltaTime;
			yield return null;
		}

		time = 0f;
		while (time < fadeTime)
		{
			time += Time.deltaTime;
			FadeVolume = Mathf.Clamp01 (time / fadeTime);
			ChangeVolume ();
			yield return null;
		}
	}
	private IEnumerator IeFadeOut (float fadeTime, float waitTime)
	{
		var time = 0f;
		while (time < waitTime)
		{
			time += Time.deltaTime;
			yield return null;
		}
		time = 0f;
		while (time < fadeTime)
		{
			time += Time.deltaTime;
			FadeVolume = 1f - Mathf.Clamp01 (time / fadeTime);
			ChangeVolume ();
			yield return null;
		}
		Stop ();
	}
	#endregion

	#region Dispose

	protected void Dispose ()
	{
		PlayDispose?.Dispose ();
	}

	protected void DisposeFade ()
	{
		FadeDispose?.Dispose ();
	}
	#endregion
}
