﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "SoundAssetInScene", menuName = "SoundAssetInScene")]
public class SoundAssetInScene : SingletonScriptableObject<SoundAssetInScene>
{
	[SerializeField] private AudioClip[] soundEffects;
	[SerializeField] private AudioClip[] backGroundMusics;

	public AudioClip[] BackGroundMusics { get => backGroundMusics; set => backGroundMusics = value; }
	public AudioClip[] SoundEffects { get => soundEffects; set => soundEffects = value; }
}