﻿using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu]
public class SoundScriptable : SingletonScriptableObject<SoundScriptable>
{
    [Range(0f, 1f)] public float volumeBgm = 0.5f;

    [Range(0f, 1f)] public float volumeSe = 0.5f;

    [Range(0f, 1f)] public float volumeTotal = 1f;

    public bool isChangeSceneStopSe;

    public bool isBgMusic;

    public bool isSoundEffect;
}