﻿using System;
using ModuleSound;
using UniRx;
using UniRx.Toolkit;
using UnityEngine;
using Object = UnityEngine.Object;

public class SoundEffectPool : AsyncObjectPool<SoundEffectPlayer>
{
	private readonly SoundEffectPlayer _soundEffect;
	private readonly Transform _rootParent;
	public SoundEffectPool(SoundEffectPlayer soundEffect, Transform rootParent)
	{
		this._soundEffect = soundEffect;
		this._rootParent = rootParent;
	}

	protected override IObservable<SoundEffectPlayer> CreateInstanceAsync ()
	{
		var sound = Object.Instantiate (_soundEffect, _rootParent, false);
		return Observable.Return (sound);
	}
}
