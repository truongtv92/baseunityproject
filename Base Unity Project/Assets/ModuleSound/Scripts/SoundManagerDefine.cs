﻿public static class SoundManagerDefine
{
	public static string PathBgmSourceFolder => "Assets/Project/Scripts/Aprius/SoundManager/Sounds/Bgm";
	public static string PathSoundEffectSourceFolder => "Assets/Project/Scripts/Aprius/SoundManager/Sounds/SoundEffect";
	public static string PathSoundName => "Assets/Project/Scripts/Aprius/SoundManager/Scripts/SoundName.cs";
}
