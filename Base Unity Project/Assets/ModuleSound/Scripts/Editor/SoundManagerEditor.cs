﻿using UnityEditor;

[CustomEditor(typeof(ModuleSound.SoundManager))]
public class SoundManagerEditor : Editor
{
    private SerializedProperty _soundEffectPrefab;
    private SerializedProperty _soundData;
    private SerializedProperty _soundAssetInScene;
    private SerializedProperty _mainBgmPlayer;
    private SerializedObject _serializeObject;

    private void OnEnable()
    {
        _serializeObject = new SerializedObject(target);
        _soundEffectPrefab = _serializeObject.FindProperty("soundEffectPrefab");
        _soundData = _serializeObject.FindProperty("soundData");
        _mainBgmPlayer = _serializeObject.FindProperty("mainBgmPlayer");
        _soundAssetInScene = _serializeObject.FindProperty("soundAssetInScene");

        EditorUtility.SetDirty(target);
        _serializeObject.ApplyModifiedProperties();
    }

    public override void OnInspectorGUI()
    {
        if (EditorTools.DrawHeader("[Setting]"))
        {
            EditorGUILayout.PropertyField(_soundEffectPrefab);
            EditorGUILayout.PropertyField(_soundData);
            EditorGUILayout.PropertyField(_soundAssetInScene);
            EditorGUILayout.PropertyField(_mainBgmPlayer);
        }

        EditorUtility.SetDirty(target);
        _serializeObject.ApplyModifiedProperties();
    }
}