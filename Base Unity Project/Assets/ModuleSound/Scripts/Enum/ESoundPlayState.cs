﻿public enum ESoundPlayState
{
	Stop,
	Playing,
	Pause,
	Await
}
