﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace ModuleSound
{
	public class BackGroundMusicPlayer : BaseAudio
	{
		#region Properties
		private ESoundPlayState _previousState;
		#endregion

		#region GetSet
		public bool IsActive => !EqualCurrentState(ESoundPlayState.Stop);
		public bool IsPlaying => EqualCurrentState(ESoundPlayState.Playing);

		public float Length
		{
			get {
				if (CurrentState == ESoundPlayState.Stop || CurrentState == ESoundPlayState.Await)
				{
					return 0f;
				}
				return Mathf.Clamp01 (Source.time / Source.clip.length);
			}
		}
		#endregion

		#region Method

		public void Init ()
		{
			ChangeCurrentState (ESoundPlayState.Stop);
			FadeVolume = 1f;
			Source = gameObject.GetComponent<AudioSource> ();
			Source.playOnAwake = false;
			Source.loop = false;
			Source.spatialBlend = 0f;
			Source.volume = SoundManager.Instance.VolumeBgm;
		}

		private IEnumerator IePlay ()
		{
			ChangeCurrentState (ESoundPlayState.Stop);
			WaitTime = 0f;
			while (WaitTime < Delay)
			{
				WaitTime += Time.deltaTime;
				yield return null;
			}

			ChangeCurrentState (ESoundPlayState.Playing);
			do
			{
				OnStart?.Invoke ();
				Source.clip = Clip;
				Source.time = 0f;
				ChangeVolume ();
				if (FadeInTime > 0.0f)
				{
					FadeIn (FadeInTime, WaitTimeFadeIn);
				}
				Source.Play ();
				WaitTime = 0f;
				while (WaitTime < Clip.length)
				{
					WaitTime += Time.deltaTime;
					yield return null;
				}
				if (!IsLoop)
				{
					ChangeCurrentState (ESoundPlayState.Stop);
				}

				OnCompleted?.Invoke ();
			} while (IsLoop);
		}

		public void Play ()
		{
			PlayDispose = Observable.FromMicroCoroutine (IePlay).Subscribe ((m) => Dispose ());
		}

		public override void Stop ()
		{
			Dispose ();
			Source.Stop ();
			DisposeFade ();
		}

		public void Pause ()
		{
			if (!(EqualCurrentState(ESoundPlayState.Playing) || EqualCurrentState(ESoundPlayState.Await)))
			{
				return;
			}

			_previousState = CurrentState;
			ChangeCurrentState (ESoundPlayState.Pause);
			Dispose ();
			Source.Pause ();
			FadeOut (FadeOutTime, WaitTimeFadeOut);
		}

		public void Resume ()
		{
			if (!EqualCurrentState(ESoundPlayState.Pause))
			{
				SoundManager.Instance.PlayBackgroundMusic (SoundManager.Instance.RandomBgm (), SoundManager.Instance.VolumeBgm, 0f, true, 0.5f, 0f, null, null);
				return;
			}
			CurrentState = _previousState;
			Play ();
			Source.Play ();
			FadeIn (FadeInTime, WaitTimeFadeIn);
		}

		protected override void ChangeVolume ()
		{
			Source.volume = Volume * FadeVolume * SoundManager.Instance.VolumeBgm * SoundManager.Instance.VolumeTotal;
		}
		#endregion
	}
}


