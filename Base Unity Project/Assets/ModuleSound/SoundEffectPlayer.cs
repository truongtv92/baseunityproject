﻿using System;
using System.Collections;
using UniRx;
using UnityEngine;

namespace ModuleSound
{
	public class SoundEffectPlayer : BaseAudio
	{
		#region GetSet
		public int LoopCount { get; set; }

		public float Length
		{
			get {
				if (CurrentState == ESoundPlayState.Stop || CurrentState == ESoundPlayState.Await)
				{
					return 0f;
				}
				return Mathf.Clamp01 (Source.time / Source.clip.length);
			}
		}

		public bool IsActive => !EqualCurrentState (ESoundPlayState.Stop);
		public bool IsPlaying => EqualCurrentState (ESoundPlayState.Playing) || EqualCurrentState (ESoundPlayState.Await);

		#endregion

		#region Method
		public void Init ()
		{
			ChangeCurrentState (ESoundPlayState.Stop);
			Source = gameObject.GetComponent<AudioSource> ();
			Source.loop = false;
			Source.playOnAwake = false;
			WaitTime = 0f;
			ResetPlayer ();
		}

		public IObservable<UniRx.Unit> Play (Func<bool> conditionStop)
		{
			gameObject.SetActive (true);
			ChangeCurrentState (ESoundPlayState.Await);
			ChangeVolume ();
			OnStartBefore?.Invoke ();
			Dispose ();
			return Observable.FromMicroCoroutine (() => IePlay (conditionStop)).ForEachAsync (_ => Dispose ());
		}

		private IEnumerator IePlay (Func<bool> fun)
		{
			if (!IsLoop)
			{
				LoopCount--;
			}
			WaitTime = 0f;
			while (WaitTime < Delay)
			{
				WaitTime += Time.deltaTime;
				yield return null;
			}
			OnStart?.Invoke ();
			ChangeCurrentState (ESoundPlayState.Playing);
			Source.Play ();
			WaitTime = 0f;

			while (WaitTime < Source.clip.length / Source.pitch)
			{
				WaitTime += Time.deltaTime;
				if (fun != null)
				{
					var check = fun ();
					if (check)
					{
						break;
					}
				}
				yield return null;
			}
			OnCompleted?.Invoke ();

			if (LoopCount <= 0)
			{
				ChangeCurrentState (ESoundPlayState.Stop);
				PlayEnd ();
				Dispose ();
				yield break;
			}
			else
			{
				Dispose ();
				PlayDispose = Observable.FromMicroCoroutine (() => IePlay (fun)).Subscribe ((m) => Dispose ());
				yield break;
			}
		}


		public override void Stop ()
		{
			if (!EqualCurrentState(ESoundPlayState.Await) && !EqualCurrentState(ESoundPlayState.Playing) && !EqualCurrentState(ESoundPlayState.Pause)) return;
			Source.Stop ();
			Dispose ();
			ResetPlayer ();
		}

		private void PlayEnd ()
		{
			OnCompletedAfter?.Invoke ();
			ResetPlayer ();
		}

		public void Resume (Func<bool> fun)
		{
			if (!EqualCurrentState(ESoundPlayState.Pause)) return;
			PlayDispose = Observable.FromMicroCoroutine (() => IePlay (fun)).Subscribe ((m) => Dispose ());
			ChangeCurrentState (ESoundPlayState.Pause);
			Source.UnPause ();
		}

		private void ResetPlayer ()
		{
			gameObject.SetActive (false);
			ChangeCurrentState (ESoundPlayState.Stop);
		}

		protected override void ChangeVolume ()
		{
			Source.volume = Volume * SoundManager.Instance.VolumeSe * SoundManager.Instance.VolumeTotal;
		}
		#endregion

	}
}

