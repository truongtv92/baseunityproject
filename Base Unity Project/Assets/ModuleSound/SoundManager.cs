﻿using System;
using System.Collections.Generic;
using ModuleSound;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ModuleSound
{
    public class SoundManager : Singleton<SoundManager>
    {
        #region Constaint

        private const bool DEFAULT_PARAM_IS_LOOP = true;
        private const float DEFAULT_PARAM_VOLUME = 1.0f;
        private const float DEFAULT_PARAM_DELAY = 0.0f;
        private const float DEFAULT_PARAM_VOLUME_TOTAL = 1.0f;
        private const float DEFAULT_PARAM_VOLUME_BGM = 0.5f;
        private const float DEFAULT_PARAM_VOLUME_SE = 0.5f;

        #endregion

        #region Properties

        public SoundEffectPlayer soundEffectPrefab;
        public SoundScriptable soundData;
        public SoundAssetInScene soundAssetInScene;
        public BackGroundMusicPlayer mainBgmPlayer;
        private SoundEffectPool _soundEffectPool;

        private Dictionary<string, AudioClip> _clipSoundEffectDicctionary = new Dictionary<string, AudioClip>();
        private Dictionary<string, AudioClip> _clipBgmDicctionary = new Dictionary<string, AudioClip>();

        #endregion

        #region GetSet

        public void InitSoundAssetInScene(SoundAssetInScene asset)
        {
            soundAssetInScene = asset;
            InitilizeClipDictionary();
        }

        public float VolumeBgm
        {
            get => soundData.volumeBgm;
            set => soundData.volumeBgm = Mathf.Clamp01(value);
        }

        public float VolumeTotal
        {
            get => soundData.volumeTotal;
            set { soundData.volumeTotal = Mathf.Clamp01(value); }
        }

        public float VolumeSe
        {
            get => soundData.volumeSe;
            set { soundData.volumeSe = Mathf.Clamp01(value); }
        }

        public bool IsPlayingBgm
        {
            get { return mainBgmPlayer.IsPlaying; }
        }

        #endregion

        #region Method

        protected override void Awake()
        {
            base.Awake();
            SceneManager.sceneUnloaded += OnSceneUnloaded;
            _soundEffectPool = new SoundEffectPool(soundEffectPrefab, transform);
            mainBgmPlayer.Init();

            InitilizeClipDictionary();
        }

        private void InitilizeClipDictionary()
        {
            _clipSoundEffectDicctionary.Clear();
            _clipBgmDicctionary.Clear();
            for (int i = 0; i < soundAssetInScene.SoundEffects.Length; i++)
            {
                _clipSoundEffectDicctionary.Add(soundAssetInScene.SoundEffects[i].name, soundAssetInScene.SoundEffects[i]);
            }

            for (int i = 0; i < soundAssetInScene.BackGroundMusics.Length; i++)
            {
                _clipBgmDicctionary.Add(soundAssetInScene.BackGroundMusics[i].name, soundAssetInScene.BackGroundMusics[i]);
            }
        }

        private void OnSceneUnloaded(Scene scene)
        {
            if (soundData.isChangeSceneStopSe)
            {
                StopSoundEffect();
                Instance._soundEffectPool.Clear();
            }
        }

        #endregion

        #region SoundEffect

        public SoundEffectPlayer PlaySoundEffect(SoundNameSe soundName, float volume = DEFAULT_PARAM_VOLUME, float delay = DEFAULT_PARAM_DELAY, Func<bool> conditionStop = null, Action onStartBefore = null, Action onStart = null, Action onComplete = null, Action onCompleteAfter = null)
        {
            return soundName == SoundNameSe.None ? null : PlaySoundEffect(soundName.ToString(), volume, delay, false, 1, 0.0f, 0.0f, false, Vector3.zero, conditionStop, onStartBefore, onStart, onComplete, onCompleteAfter);
        }

        public SoundEffectPlayer PlaySoundEffectLoop(SoundNameSe soundName, int loopCount, float volume = DEFAULT_PARAM_VOLUME, float delay = DEFAULT_PARAM_DELAY, Func<bool> conditionStop = null, Action onStartBefore = null, Action onStart = null, Action onComplete = null, Action onCompleteAfter = null)
        {
            return soundName == SoundNameSe.None ? null : PlaySoundEffect(soundName.ToString(), volume, delay, false, loopCount, 0.0f, 0.0f, false, Vector3.zero, conditionStop, onStartBefore, onStart, onComplete, onCompleteAfter);
        }

        public SoundEffectPlayer PlaySoundEffectLoopInfinity(SoundNameSe soundName, float volume = DEFAULT_PARAM_VOLUME, float delay = DEFAULT_PARAM_DELAY, Func<bool> conditionStop = null, Action onStartBefore = null, Action onStart = null, Action onComplete = null, Action onCompleteAfter = null)
        {
            return soundName == SoundNameSe.None ? null : PlaySoundEffect(soundName.ToString(), volume, delay, true, 1, 0.0f, 0.0f, false, Vector3.zero, conditionStop, onStartBefore, onStart, onComplete, onCompleteAfter);
        }

        public SoundEffectPlayer PlaySoundEffectFadeInOut(SoundNameSe soundName, float fadeInTime, float fadeOutTime, float volume = DEFAULT_PARAM_VOLUME, float delay = DEFAULT_PARAM_VOLUME, Func<bool> conditionStop = null, Action onStartBefore = null, Action onStart = null, Action onComplete = null, Action onCompleteAfter = null)
        {
            return soundName == SoundNameSe.None ? null : PlaySoundEffect(soundName.ToString(), volume, delay, false, 1, fadeInTime, fadeOutTime, false, Vector3.zero, conditionStop, onStartBefore, onStart, onComplete, onCompleteAfter);
        }

        private SoundEffectPlayer PlaySoundEffect(
            string soundName,
            float volume,
            float delay,
            bool isLoopInfinity,
            int loopCount,
            float fadeInTime,
            float fadeOutTime,
            bool is3DSound,
            Vector3 soundPosition,
            Func<bool> conditionStop,
            Action onStartBefore,
            Action onStart,
            Action onComplete,
            Action onCompleteAfter)
        {
            if (!soundData.isSoundEffect)
            {
                return null;
            }

            if (!_clipSoundEffectDicctionary.ContainsKey(soundName))
            {
#if UNITY_EDITOR
                Debug.LogWarning("Sound effect [" + soundName + "] not exist in dictionary!");
#endif
                return null;
            }

            AudioClip clip = _clipSoundEffectDicctionary[soundName];
            float spatialBlend = is3DSound ? 1f : 0f;

            _soundEffectPool.RentAsync().Subscribe(sound =>
            {
                sound.Init();
                sound.Source.clip = clip;
                sound.transform.position = soundPosition;
                sound.Source.spatialBlend = spatialBlend;
                sound.Source.rolloffMode = is3DSound ? AudioRolloffMode.Linear : AudioRolloffMode.Logarithmic;
                sound.LoopCount = loopCount;
                sound.Volume = volume;
                sound.Delay = delay;

                sound.OnStartBefore = onStartBefore;
                sound.OnStart = onStart;
                sound.OnCompleted = onComplete;
                sound.OnCompletedAfter = onCompleteAfter;

                fadeInTime = Mathf.Clamp(fadeInTime, 0f, float.MaxValue);
                fadeOutTime = Mathf.Clamp(fadeOutTime, 0f, float.MaxValue);
                sound.IsLoop = isLoopInfinity;
                sound.Play(conditionStop).Subscribe(_ => _soundEffectPool.Return(sound));
            });
            return null;
        }

        private static void StopSoundEffect()
        {
        }

        public void PauseSoundEffect()
        {
        }

        public void ResumeSoundEffect()
        {
        }

        #endregion

        #region BGM

        public BackGroundMusicPlayer PlayBackgroundMusic(SoundNameBgm mainSoundName, bool isLoop = DEFAULT_PARAM_IS_LOOP, float volume = DEFAULT_PARAM_VOLUME, float delay = DEFAULT_PARAM_DELAY, Action onMainStart = null, Action onMainComplete = null)
        {
            return PlayBackgroundMusic(mainSoundName.ToString(), volume, delay, isLoop, 0.0f, 0.0f, onMainStart, onMainComplete);
        }

        public BackGroundMusicPlayer PlayBackgroundMusicFadeInOut(SoundNameBgm mainSoundName, float fadeInTime, float fadeOutTime, bool isLoop = DEFAULT_PARAM_IS_LOOP, float volume = DEFAULT_PARAM_VOLUME, float delay = DEFAULT_PARAM_DELAY, Action onMainStart = null, Action onMainComplete = null)
        {
            return PlayBackgroundMusic(mainSoundName.ToString(), volume, delay, isLoop, fadeInTime, fadeOutTime, onMainStart, onMainComplete);
        }

        public BackGroundMusicPlayer PlayIntro(SoundNameBgm mainSoundName, bool isLoop = DEFAULT_PARAM_IS_LOOP, float volume = DEFAULT_PARAM_VOLUME, float delay = DEFAULT_PARAM_DELAY, Action onMainStart = null, Action onMainComplete = null)
        {
            return PlayBackgroundMusic(mainSoundName.ToString(), volume, delay, isLoop, 0.0f, 0.0f, onMainStart, onMainComplete);
        }

        public BackGroundMusicPlayer PlayIntroFadeInOut(SoundNameBgm mainSoundName, float fadeInTime, float fadeOutTime, bool isLoop = DEFAULT_PARAM_IS_LOOP, float volume = DEFAULT_PARAM_VOLUME, float delay = DEFAULT_PARAM_DELAY, Action onMainStart = null, Action onMainComplete = null)
        {
            return PlayBackgroundMusic(mainSoundName.ToString(), volume, delay, isLoop, fadeInTime, fadeOutTime, onMainStart, onMainComplete);
        }

        public BackGroundMusicPlayer PlayBackgroundMusic(
            string mainSoundName,
            float volume,
            float delay,
            bool isLoop,
            float fadeInTime,
            float fadeOutTime,
            Action onMainStart,
            Action onMainComplete)
        {
            if (!soundData.isBgMusic)
            {
                return null;
            }

            AudioClip mainClip = null;
            if (!_clipBgmDicctionary.ContainsKey(mainSoundName))
            {
#if UNITY_EDITOR
                Debug.Log("BGM with that name does not exist :" + mainSoundName);
#endif
                return null;
            }

            volume = Mathf.Clamp01(volume);
            mainClip = _clipBgmDicctionary[mainSoundName];

            StopBgm(); // stop current music
            mainBgmPlayer.FadeInTime = fadeInTime;
            mainBgmPlayer.FadeOutTime = fadeOutTime;
            mainBgmPlayer.Clip = mainClip;
            mainBgmPlayer.IsLoop = isLoop;
            mainBgmPlayer.Delay = delay;
            mainBgmPlayer.Volume = volume;
            mainBgmPlayer.OnStart = onMainStart;
            mainBgmPlayer.OnCompleted = onMainComplete;
            mainBgmPlayer.Play();
            return mainBgmPlayer;
        }

        private void StopBgm()
        {
            if (mainBgmPlayer.CurrentState == ESoundPlayState.Stop)
            {
                return;
            }

            mainBgmPlayer.ChangeCurrentState(ESoundPlayState.Stop);
            if (mainBgmPlayer.FadeOutTime > 0.0f)
            {
                mainBgmPlayer.FadeOut(mainBgmPlayer.FadeOutTime, mainBgmPlayer.WaitTimeFadeOut);
                return;
            }

            mainBgmPlayer.Stop();
        }

        public void PauseBgm()
        {
            mainBgmPlayer.Pause();
        }

        public void ResumeBgm()
        {
            mainBgmPlayer.Resume();
        }

        public string RandomBgm()
        {
            return soundAssetInScene.BackGroundMusics[UnityEngine.Random.Range(0, soundAssetInScene.BackGroundMusics.Length)].name;
        }

        #endregion

        #region Persident Save

        private bool _isPaused = false;

        //private string _persisterName = "Tripz";
        private void OnApplicationFocus(bool hasFocus)
        {
            _isPaused = !hasFocus;
        }

        private void OnApplicationPause(bool pauseStatus)
        {
//			UltiGenerics.SavePersident (persisterName, SoundData);
            _isPaused = pauseStatus;
        }

        private void OnEnable()
        {
//			UltiGenerics.LoadPersident (persisterName, SoundData);
        }

        private void OnDisable()
        {
//			UltiGenerics.SavePersident (persisterName, SoundData);
        }

        #endregion
    }
}

[Serializable]
public struct AudioSeStruct
{
    public SoundNameSe name;
    [Range(0f, 1f)] public float volume;
    public float timeDelay;
}