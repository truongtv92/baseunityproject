﻿using UnityEngine;

public class LockScene : MonoBehaviour
{
    private static GameObject lockSceneObject;
    private void Awake()
    {
        lockSceneObject = gameObject;
    }
    public static void SetActive(bool active)
    {
        lockSceneObject.SetActive(active);
    }
}
