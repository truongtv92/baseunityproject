﻿using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Language_Manager
{
    public class LanguageTextManager : MonoBehaviour
    {
        [FormerlySerializedAs("LanguageId")] [SerializeField]
        private string languageId;

        [CanBeNull] private object[] _param;
        private Text _text;
        private TMP_Text _txTmpText;
        private TextMesh _textMesh;

        private void Awake()
        {
            _text = GetComponent<Text>();
            _txTmpText = GetComponent<TMP_Text>();
            _textMesh = GetComponent<TextMesh>();
        }
        private void Start()
        {
            if (_param != null && _param.Length == 0) _param = null;
            SetText();
        }
        public void ResetText()
        {
            SetText();
        }
        private async void SetText()
        {
            if(languageId.Equals("")) return;
            var languageText = await LanguageManager.GetText(languageId);
            if (languageText.Equals("BAD_KEY"))
            {
                if (_txTmpText != null)
                    _txTmpText.text = languageText;
                if (_text != null)
                    _text.text = languageText;
                if (_textMesh != null)
                    _textMesh.text = languageText;
            }
            else
            {
                if (_txTmpText != null)
                    _txTmpText.text = _param == null ? languageText : string.Format(languageText, _param);
                if (_text != null)
                    _text.text = _param == null ? languageText : string.Format(languageText, _param);
                if (_textMesh != null)
                    _textMesh.text = _param == null ? languageText : string.Format(languageText, _param);
            }
        }
        public void SetLanguageId(string key, object[] param = null)
        {
            languageId = key;
            _param = param;
            SetText();
        }
    }
}