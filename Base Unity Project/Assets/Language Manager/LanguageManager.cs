using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UniRx.Async;
using UnityEngine;

namespace Language_Manager
{
	[Serializable]
	public class LanguagePack
	{
		private readonly Dictionary<string,string> _languageData = new Dictionary<string, string>();
		public void AddNewString(string key, string text)
		{
			if (_languageData.TryGetValue(key, out _)) return;
			_languageData.Add(key,text);
		}
		public string GetString (string key)
		{
			return _languageData.TryGetValue(key, out var value)? value: "BAD_KEY";
		}
	}

	public class LanguageManager:Singleton<LanguageManager>
	{
		private static bool packLoaded;
		private static LanguagePack CurrentLanguagePack { get; set; }
		public Language currentLanguage;
		//static XmlSerializer serializer;

		private new void Awake()
		{
			base.Awake();
			var saveLanguage = PlayerPrefs.GetString("language");
			if (saveLanguage.Equals(""))
				Init(currentLanguage);
			else
				Init(Enum.TryParse<Language>(saveLanguage, true, out var language) ? language : currentLanguage);
		}
		private void Init(Language language)
		{
			currentLanguage = language;
			LoadLanguageFile(currentLanguage);
		}

		public void ChangeLanguage(Language language)
		{
			if (currentLanguage != language)
			{
				PlayerPrefs.SetString("language",language.ToString());
				PlayerPrefs.Save();
				currentLanguage = language;
				LoadLanguageFile(currentLanguage);
				ReloadTextInScene();
			}
			else
			{
				currentLanguage = language;
				LoadLanguageFile(currentLanguage);
			}
		}
		private void ReloadTextInScene()
		{
			foreach (var languageTextManager in GetComponents<LanguageTextManager>())
			{
				languageTextManager.ResetText();
			}
		}
		public static async Task<string> GetText (string key)
		{
			await UniTask.WaitUntil(() => packLoaded);
			return CurrentLanguagePack.GetString (key).Replace ("\\n", Environment.NewLine);
		}

		private static async void LoadLanguageFile (Language language)
		{
			CurrentLanguagePack = await LoadToPack (language);
		}
		private static async Task<LanguagePack> LoadToPack (Language language)
		{
			try
			{
				var loadName = Enum.GetName (typeof (Language), language);
				var filePath = "Languages/" + loadName;
				var pk = new LanguagePack ();
				var xmlFile = (TextAsset) Resources.Load(filePath, typeof (TextAsset));
				var t = xmlFile.text;

				using (var f = new StringReader (t))
				{
					string line;
					while ((line = await f.ReadLineAsync ()) != null)
					{
						if (line.Length > 0)
							pk.AddNewString (line.Split (new[] { " = " }, StringSplitOptions.None)[0], line.Split (new[] { " = " }, StringSplitOptions.None)[1]);
					}
				}
				packLoaded = true;

				return pk;
			}
			catch
			{
#if UNITY_EDITOR
				Logger.Log ("No File Was Found! Make sure the language file you are trying to load exists!");
#endif
				packLoaded = false;
				return null;
			}
		}
	}
}